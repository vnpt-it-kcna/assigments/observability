# Create resources
kubectl apply -f namespace.yaml
kubectl apply -f prometheus/
kubectl apply -f kube-state-metrics/
kubectl apply -f node-exporter/
kubectl apply -f grafana/

# Add localhost: [minikube ip] grafana.minikube
# Access grafana.minikube via browser

# Create prometheus datasouce with http://prometheus:9090